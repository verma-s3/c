﻿using System;

namespace SingletonExample
{
    class Program
    {
        static void Main(string[] args)
        {
            Singlton firstinstance = Singlton.GetInstance();

            Singlton secondinstance = Singlton.GetInstance();

            if (firstinstance == secondinstance) {
                Console.WriteLine("Ther is only one instance");
            }
        }
    }
}
