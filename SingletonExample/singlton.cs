﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SingletonExample
{
    class Singlton
    {
        private Singlton()
        { 
        
        }

        public static Singlton _instance;

        public static Singlton GetInstance()
        {
            if (_instance == null) {
                _instance = new Singlton();
            }
            return _instance;
        }
    }
}
