﻿using System;
using System.Text;

namespace string_stringBuilder_example
{
    class Program
    {
        
        static void Main(string[] args)
        {
            string firstname = "sonia";

            firstname = "nisha";
            System.String lastname = "verma";
            String jobtitle = "web developer";
            var address = "winnipeg";
            string emptyString = String.Empty;
            string nullString = null;

            char[] letter = { 'A', 'B', 'C' };
            string alpabet = new string(letter);

            var fullname = "sonia verma";

            var myfirstname = fullname.Substring(0, 4);
            Console.WriteLine(myfirstname);

            var mylastname = fullname.Substring(4);
            Console.WriteLine(mylastname);
            for (int i = fullname.Length-1; i >= 0; i--) {
                Console.WriteLine(fullname[i]);
            }

            StringBuilder kidname = new StringBuilder("nisha verma");

            for (int i = kidname.Length - 1;i>=0; i++)
            {
                kidname[i] = System.Char.ToUpper(kidname[i]);
            }

            else if (System.Char.IsUpper(kidname[i]) == true)
            {
                kidname[i] = System.Char.ToLower(kidname[i]);
            }
        }
    }
}
