﻿using System;

namespace interfaceExample
{
    class Program
    {
        static void Main(string[] args)
        {
            var myCar = new Car();
            myCar.Start();

            var myTruck = new Truck();
            myTruck.Start();
        }
    }
}
