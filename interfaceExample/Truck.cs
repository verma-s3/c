﻿using System;
using System.Collections.Generic;
using System.Text;

namespace interfaceExample
{
    class Truck: IVehicle
    {
        public bool Start()
        {
            Console.WriteLine("Truck Started");
            return true;
        }
    }
}
