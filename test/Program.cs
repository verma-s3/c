﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Reflection.Metadata.Ecma335;

namespace test
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Console.WriteLine("Enter the First Number");
            var num1 = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter the Second Number");
            var num2 = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter the Third Number");
            var num3 = int.Parse(Console.ReadLine());
            var result = num1 + num2 + num3;
            double avg = result/3;
            Console.WriteLine("Your Result is: " + result );
            Console.WriteLine("Average is: " + avg);
        }
    }
}
