﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Transactions;

namespace array
{
    class Program
    {
        static void Main(string[] args)
        {
            //array intilizatin
            int[] total = new int[5];
            total[0] = 12;
            total[1] = 13;

            //Console.WriteLine(total[0]);

            //calculate sum of array numbers in a loop
            int sum = 0;
            int[] number = new int[10];
            
            for (int i = 1; i <= 10; i++)
            {
                Console.WriteLine("enter {0} number for sum ",i);
                int num;
                string user_type_number = Console.ReadLine();
                bool isNumber= int.TryParse(user_type_number, out num);
                // condition to check number validaition
                if (isNumber)
                {
                    number[i - 1] = num;
                    sum = number[i - 1] + sum;
                }
                else {
                    Console.WriteLine("enter valid numbers only");
                    Console.WriteLine("Enter the number {0} again.", i);
                    //user_type_number = Console.ReadLine();
                }
            }
            Console.WriteLine("sum is: {0}", sum);

        }
    }
}
