﻿using System;

namespace if_else_with_days
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the number to see Day of the week");
            // storing the number into a variable 
            var num = int.Parse(Console.ReadLine());

            // if else condition to check num value and displying result according to it.
            if (num == 1) 
            {
                Console.WriteLine("Monday");
            }
            else if(num == 2)
            {
                Console.WriteLine("Tuesday");
            }
            else if (num == 3)
            {
                Console.WriteLine("Wednesday");
            }
            else if (num == 4)
            {
                Console.WriteLine("Thursday");
            }
            else if (num == 5)
            {
                Console.WriteLine("Friday");
            }
            else if (num == 6)
            {
                Console.WriteLine("Saturday");
            }
            else if (num == 7)
            {
                Console.WriteLine("Sunday");
            }
            else
            {
                Console.WriteLine("This is not the Valid Number. Enter the number between 1 to 7 to see the correct result.");
            }
        }
    }
}
