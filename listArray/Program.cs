﻿using System;
using System.Collections.Generic;

namespace listArray
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> listOfNames = new List<string>()
            { 
                "sonia",
                "nisha"
            };

            listOfNames.Add("jiya");
            listOfNames.Insert(1,"sapna");
            listOfNames.AddRange(new string[] { "kamal", "Daksh" });

            listOfNames.Remove("sonia");
            listOfNames.RemoveAt(4);

            for (int i = 0; i < listOfNames.Count; i++)
            {
                Console.WriteLine("Name[{0}] : Name[{1}]",i,listOfNames[i]);
            }

            foreach (var name in listOfNames)
            {
                Console.WriteLine(name);
            }
        }
    }
}
