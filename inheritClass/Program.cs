﻿using System;

namespace inheritClass
{
    class Person
    {
        public string FirstName;
        public string LastName;
        private bool isWaking;
        private bool isSleeping;
        private bool isEating;

        public Person()
        {
            FirstName = "Unassigned";
            LastName = "Unassigned";
            isWaking = false;
            isEating = false;
            isSleeping = false;

        }

        public Person(string firstname, string lastname)
        {
            FirstName = "firstname";
            LastName = "lastname";
            isWaking = false;
            isEating = false;
            isSleeping = false;
        }

        public void Walk()
        {
            isWaking = true; 
        }

        public void Eat()
        {
            isEating = true;
        }

        public void Sleep()
        {
            isSleeping = true;
        }

        public void Display()
        {
            Console.WriteLine($"First Name: {FirstName} \n " + 
                $"Last name: {LastName}" +
                $"Is Walking: {isWaking}" +
                $"Is Eating: {isEating}" +
                $"Is Sleeping: {isSleeping}"
                );
        }
    }
}
