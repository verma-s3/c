﻿using System;
using System.Collections.Generic;

namespace dictionary
{
    class Program
    {
        static void Main(string[] args)
        {
            // Console.WriteLine("Hello World!");

            Dictionary<int, string> users = new Dictionary<int, string>();
            users.Add(1, "sonia");
            users.Add(2, "nisha");

            Console.WriteLine(users[1]);
        }
    }
}
